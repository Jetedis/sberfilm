package com.filmography.filmography.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.np.sber.filmCollection.dto.AddFilmDto;
import com.np.sber.filmCollection.dto.DirectorDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Slf4j
public class DirectorControllerTest extends GenericControllerTest {

    private static final String BASE_URL = "/rest/directors";

    @Override
    @Test
    void getById() {

    }

    @Override
    @Test
    void getByCreator() {

    }

    @Test
    void getAll() throws Exception {
        String result = super.mvc.perform(get(BASE_URL + "/getAll")
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        log.info(result);
        List<DirectorDto> directorDtoList = objectMapper.readValue(result, new TypeReference<List<DirectorDto>>() {
        });
        directorDtoList.forEach(directorDto -> log.info(directorDto.getDirectorsFio()));
    }

    @Test
    void create() throws Exception {
        DirectorDto directorDTO = new DirectorDto("REST_TestDirectorsFio",  "Test Description", new HashSet<>());
        String result = mvc.perform(post(BASE_URL + "/add")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(headers)
                        .content(asJsonString(directorDTO))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        DirectorDto created = objectMapper.readValue(result, DirectorDto.class);
        log.info(String.valueOf(created.getId()));
    }

    @Test
    void update() throws Exception {

        DirectorDto existingDirector = objectMapper.readValue(mvc.perform(get(BASE_URL + "/getById")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(headers)
                                .param("id", String.valueOf(33))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                DirectorDto.class);

        existingDirector.setDirectorsFio("UPDATED");
        mvc.perform(put("/rest/directors/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(headers)
                        .param("id", String.valueOf(existingDirector.getId()))
                        .content(asJsonString(existingDirector))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is2xxSuccessful());
    }

    @Override
    @Test
    void deleteObject() {
    }

    @Test
    protected void softDelete() throws Exception {
        mvc.perform(delete(BASE_URL + "/soft-delete/{id}", 34)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(headers)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());

        DirectorDto existingDirector = objectMapper.readValue(
                mvc.perform(get(BASE_URL + "/getById")
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .headers(headers)
                                .param("id", String.valueOf(34))
                        )
                        .andExpect(status().is2xxSuccessful())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(), DirectorDto.class);
        assertTrue(existingDirector.isDeleted());
    }

    @Test
    protected void restore() throws Exception {
        mvc.perform(put(BASE_URL + "/restore/{id}", 34)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(headers)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());

        DirectorDto existingDirector = objectMapper.readValue(
                mvc.perform(get(BASE_URL + "/getById")
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .headers(headers)
                                .param("id", String.valueOf(34))
                        )
                        .andExpect(status().is2xxSuccessful())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(), DirectorDto.class);
        assertFalse(existingDirector.isDeleted());
    }

    @Test
    void addFilm() throws Exception {
        AddFilmDto addFilmDto = new AddFilmDto(52, 31);
        String result = mvc.perform(post(BASE_URL + "/addFilm")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .headers(headers)
                        .content(asJsonString(addFilmDto))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        DirectorDto directorDto = objectMapper.readValue(result, DirectorDto.class);
        assertTrue(directorDto.getFilmsId().contains(52));
    }

    protected String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
