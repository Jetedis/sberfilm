package com.filmography.filmography.MVC.Controller;

import com.np.sber.filmCollection.dto.DirectorDto;
import com.np.sber.filmCollection.mapper.DirectorMapper;
import com.np.sber.filmCollection.service.DirectorService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
@Rollback(value = false)
public class MVCDirectorControllerTest extends CommonTestMVC {

    //Создаем нового режиссера для создания через контроллер (тест дата)
    private final DirectorDto directorDTO = new DirectorDto("MVC_TestDirectorsFio", "Test Position", new HashSet<>());
    private final DirectorDto directorDTOUpdated = new DirectorDto("MVC_TestDirectorsFio_UPDATED", "Test Position", new HashSet<>());

    @Autowired
    private DirectorService directorService;

    @Autowired
    private DirectorMapper mapper;

    @Test
    @DisplayName("Получение всех режиссеров")
    protected void listAll() throws Exception {
        mvc.perform(get("/directors")
                        .param("page", "1")
                        .param("size", "5")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("directors/viewAllDirectors"))
                .andExpect(model().attributeExists("directors"))
                .andReturn();
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void createObject() throws Exception {
        mvc.perform(post("/directors/add")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("directorForm", directorDTO)
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrlTemplate("/directors"));
    }


    @Test
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению режиссера через MVC начат успешно");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "directorsFio"));
        DirectorDto foundDirectorForUpdate = mapper.toDto(directorService.searchDirectors("REST_TestDirectorsFio", pageRequest).getContent().get(0));
        foundDirectorForUpdate.setDirectorsFio(directorDTOUpdated.getDirectorsFio());
        mvc.perform(post("/directors/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("directorForm", foundDirectorForUpdate)
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrl("/directors"));
        log.info("Тест по обновлению режиссера через MVC закончен успешно");
    }

    @Override
    protected void deleteObject() throws Exception {

    }
}
