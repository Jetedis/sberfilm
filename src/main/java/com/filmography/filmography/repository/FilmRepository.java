package com.filmography.filmography.repository;

import com.filmography.filmography.general.GenericRepository;
import com.filmography.filmography.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface FilmRepository extends GenericRepository <Film> {
    List<Film> findAllById(Iterable<Long> ids);

    List<Film> findByFilmTitle(String filmTitle);

    List<Film> findByFilmTitleAndCountry(String filmTitle, String country);

    List<Film> findByFilmTitleOrCountry(String filmTitle, String country);



    @Query(nativeQuery = true,
            value = """
          select distinct b.*
          from films b
          left join film_directors ba on b.id = ba.film_id
          join directors a on a.id = ba.director_id
          where b.title ilike '%' || coalesce(:title, '%') || '%'
          and cast(b.genre as char) like coalesce(:genre,'%')
          and a.directors_fio ilike '%' || :directors_fio || '%'  
          and b.is_deleted = false
               """)
    Page<Film> searchFilms(
            @Param(value = "genre") String genre,
            @Param(value = "title") String title,
            @Param(value = "directors_fio") String directors_fio,
            Pageable pageable);
}

