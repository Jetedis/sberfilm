package com.filmography.filmography.repository;

import com.filmography.filmography.general.GenericRepository;
import com.filmography.filmography.model.Director;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface DirectorRepository extends GenericRepository <Director> {
    List<Director> findAllById(Iterable<Long> ids);
    @Query(value = "select * from directors where directorsFio = ?1 and position = ?2", nativeQuery = true)
    List<Director> customQuery(String a, String b);


    @Query("""
          select case when count(a) > 0 then false else true end
          from Director a join a.films b
                        join Order bri on b.id = bri.film.id
          where a.id = :directorId
          and bri.purchase = false
          """)
    boolean checkDirectorForDeletion(final Long directorId);

    Page<Director> findAllByDirectorsFioContainsIgnoreCaseAndIsDeletedFalse(String fio, Pageable pageable);

}
