package com.filmography.filmography.repository;

import com.filmography.filmography.general.GenericRepository;
import com.filmography.filmography.model.User;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User> {
    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User findUserByChangePasswordToken(String uuid);
}

