package com.filmography.filmography.repository;

import com.filmography.filmography.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository <Role, Long> {
    Role getRoleByTitle(String title);
}