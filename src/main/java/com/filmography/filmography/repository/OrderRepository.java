package com.filmography.filmography.repository;

import com.filmography.filmography.general.GenericRepository;
import com.filmography.filmography.model.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends GenericRepository<Order> {
    List<Order> findAllByUserId(Long id);
    List<Order> findAllById(Iterable<Long> ids);
}
