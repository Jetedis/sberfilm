package com.filmography.filmography.controller;

import com.filmography.filmography.dto.AddDirectorToFilmDto;
import com.filmography.filmography.dto.CreateFilmDto;
import com.filmography.filmography.dto.FilmDto;
import com.filmography.filmography.dto.UpdateFilmDto;
import com.filmography.filmography.mapper.CreateFilmMapper;
import com.filmography.filmography.mapper.FilmMapper;
import com.filmography.filmography.mapper.UpdateFilmMapper;
import com.filmography.filmography.model.Director;
import com.filmography.filmography.model.Film;
import com.filmography.filmography.service.DirectorService;
import com.filmography.filmography.general.GenericController;
import com.filmography.filmography.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;


@Tag(name = "Фильм", description = "Контроллер для работы с фильмами")
@RestController
@RequestMapping("/films")
@Slf4j
@SecurityRequirement(name = "Bearer Authentication")
public class FilmController extends GenericController<Film, FilmDto, CreateFilmDto, UpdateFilmDto> {

    private final FilmService service;

    private final FilmMapper mapper;

    private final DirectorService directorService;

    public FilmController(
            FilmService service,
            FilmMapper mapper,
            CreateFilmMapper createFilmMapper,
            UpdateFilmMapper updateFilmMapper,
            DirectorService directorService
    ) {
        super(service, mapper);
        this.service = service;
        this.mapper = mapper;
        this.directorService = directorService;
    }

    @Operation(description = "Добавить режиссера к фильму")
    @PatchMapping(
            path = "/{id}/add-director",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FilmDto> addDirector(
            @PathVariable Long id,
            @RequestBody AddDirectorToFilmDto obj,
            HttpServletRequest request
    ) {
        log.info("Добавить режиссера к фильму: {}", request.getRequestURL());
        log.info("obj: {}", obj);
        Film film = service.findById(id);
        Director director = directorService.findById(obj.getDirectorId());
        film.getDirectors().add(director);
        service.update(film);
        return ResponseEntity.ok(mapper.toDto(film));
    }
}

