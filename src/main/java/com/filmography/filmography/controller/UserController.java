package com.filmography.filmography.controller;

import com.filmography.filmography.configuration.jwt.JWTTokenUtil;
import com.filmography.filmography.dto.*;
import com.filmography.filmography.mapper.*;
import com.filmography.filmography.model.Film;
import com.filmography.filmography.general.GenericController;
import com.filmography.filmography.model.Order;
import com.filmography.filmography.model.User;
import com.filmography.filmography.service.UserService;
import com.filmography.filmography.service.userDetails.CustomUserDetailsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Tag(name = "Пользователь", description = "Контроллер для работы с пользователями")
@RestController
@RequestMapping("/rest/users")
@Slf4j
@SecurityRequirement(name = "Bearer Authentication")
public class UserController extends GenericController<User, UserDto, CreateUserDto, UpdateUserDto> {

    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;
    private final UserService service;

    public UserController(
            UserService service,
            UserMapper mapper,
            CustomUserDetailsService customUserDetailsService,
            JWTTokenUtil jwtTokenUtil
    ) {
        super(service, mapper);
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.service = service;
    }

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody CreateUserDto createUserDto) {
        Map<String, Object> response = new HashMap<>();
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(createUserDto.getLogin());
        if(!service.checkPassword(createUserDto.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\n Неверный пароль!");
        }

        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("authorities", foundUser.getAuthorities());
        return ResponseEntity.ok().body(response);
    }
}







//    private final UserService service;
//
//    private final UserWithOrdersMapper userWithOrdersMapper;
//
//    private final FilmMapper filmMapper;
//
//    public UserController(
//            UserService service,
//            UserMapper mapper,
//            CreateUserMapper createUserMapper,
//            UpdateUserMapper updateUserMapper,
//            UserWithOrdersMapper userWithOrdersMapper,
//            FilmMapper filmMapper
//    ) {
//        super(service, mapper, createUserMapper, updateUserMapper);
//        this.service = service;
//        this.userWithOrdersMapper = userWithOrdersMapper;
//        this.filmMapper = filmMapper;
//    }
//
//    @Operation(description = "Получить список пользователей со списком заказов")
//    @GetMapping(path = "/with-orders", produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<List<UserWithOrdersDto>> findAllWithOrders(HttpServletRequest request) {
//        log.info("Получить все записи: {}", request.getRequestURL());
//        return ResponseEntity.ok(userWithOrdersMapper.toDtos(service.findAll()));
//    }
//
//    @Operation(description = "Получить список всех фильмов арендованных/купленных пользователем")
//    @GetMapping(
//            path = "/{id}/rented-films",
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public ResponseEntity<List<FilmDto>> findAllRentedFilmsByUser(
//            @PathVariable Long id,
//            HttpServletRequest request
//    ) {
//        log.info("Получить список всех фильмов арендованных/купленных пользователем: {}", request.getRequestURL());
//        User user = service.findById(id);
//        Set<Order> orders = user.getOrders();
//        List<Film> films = orders.stream().map(o -> o.getFilm()).toList();
//        return ResponseEntity.ok(filmMapper.toDtos(films));
//    }
//}
//
