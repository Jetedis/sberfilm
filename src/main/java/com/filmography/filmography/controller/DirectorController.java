package com.filmography.filmography.controller;

import com.filmography.filmography.model.Film;
import com.filmography.filmography.service.FilmService;
import com.filmography.filmography.general.GenericController;
import com.filmography.filmography.dto.AddFilmToDirectorDto;
import com.filmography.filmography.dto.CreateDirectorDto;
import com.filmography.filmography.dto.DirectorDto;
import com.filmography.filmography.dto.UpdateDirectorDto;
import com.filmography.filmography.mapper.CreateDirectorMapper;
import com.filmography.filmography.mapper.DirectorMapper;
import com.filmography.filmography.mapper.UpdateDirectorMapper;
import com.filmography.filmography.model.Director;
import com.filmography.filmography.service.DirectorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Tag(name = "Режиссёр", description = "Контроллер для работы с режисcёрами")
@RestController
@SecurityRequirement(name = "Bearer Authentication")
@RequestMapping("/rest/directors")
@Slf4j
public class DirectorController extends GenericController<Director, DirectorDto, CreateDirectorDto, UpdateDirectorDto> {

    private final DirectorService service;

    private final DirectorMapper mapper;

    private final FilmService filmService;

    public DirectorController(
            DirectorService service,
            DirectorMapper mapper,
            CreateDirectorMapper createDirectorMapper,
            UpdateDirectorMapper updateDirectorMapper,
            FilmService filmService
    ) {
        super(service, mapper);
        this.service = service;
        this.mapper = mapper;
        this.filmService = filmService;
    }

    @Operation(description = "Добавить фильм режиссёру")
    @PatchMapping(
            path = "/{id}/add-film",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<DirectorDto> addFilm(
            @PathVariable Long id,
            @RequestBody AddFilmToDirectorDto obj,
            HttpServletRequest request
    ) {
        log.info("Добавить фильм режиссёру: {}", request.getRequestURL());
        log.info("obj: {}", obj);
        Director director = service.findById(id);
        Film film = filmService.findById(obj.getFilmId());
        film.getDirectors().add(director);
        filmService.update(film);
        return ResponseEntity.ok(mapper.toDto(director));
    }
}
