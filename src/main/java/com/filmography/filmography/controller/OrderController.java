package com.filmography.filmography.controller;

import com.filmography.filmography.dto.CreateOrderDto;
import com.filmography.filmography.dto.OrderDto;
import com.filmography.filmography.dto.UpdateOrderDto;
import com.filmography.filmography.general.GenericController;
import com.filmography.filmography.mapper.CreateOrderMapper;
import com.filmography.filmography.mapper.OrderMapper;
import com.filmography.filmography.mapper.UpdateOrderMapper;
import com.filmography.filmography.model.Order;
import com.filmography.filmography.service.OrderService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Заказ", description = "Контроллер для работы с заказами")
@RestController
@RequestMapping("/orders")
@SecurityRequirement(name = "Bearer Authentication")
public class OrderController extends GenericController<Order, OrderDto, CreateOrderDto, UpdateOrderDto> {

    public OrderController(
            OrderService service,
            OrderMapper mapper,
            CreateOrderMapper createOrderMapper,
            UpdateOrderMapper updateOrderMapper
    ) {
        super(service, mapper);
    }
}