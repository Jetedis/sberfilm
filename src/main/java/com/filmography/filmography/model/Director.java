package com.filmography.filmography.model;

import com.filmography.filmography.general.GenericModel;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "directors")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "director_seq", allocationSize = 1)

public class Director extends GenericModel {

    @Column(name = "directors_fio", nullable = false)
    private String directorsFio;

    @Column(name = "position", nullable = false)
    private String position;

    @ManyToMany(mappedBy = "directors", fetch = FetchType.LAZY)
    private Set<Film> films = new HashSet<>();
}

