package com.filmography.filmography.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor

public enum Genre {

        ACTION ("Боевик"),
        COMEDY ("Комедия"),
        DRAMA("Драма"),
        FANTASTIC("Фантастика"),
        THRILLER("Триллер"),
       DETECTIVE ("Детектив");

        private final String value;

}
