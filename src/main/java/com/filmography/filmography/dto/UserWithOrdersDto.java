package com.filmography.filmography.dto;

import com.filmography.filmography.general.GenericDto;
import com.filmography.filmography.dto.OrderDto;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@Data
public class UserWithOrdersDto extends GenericDto {
    private String login;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    private String phone;
    private String address;
    private String email;
    private LocalDateTime createdWhen;
    private Long roleId;
    private List<OrderDto> orders;
}