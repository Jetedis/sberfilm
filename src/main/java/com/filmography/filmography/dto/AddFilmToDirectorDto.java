package com.filmography.filmography.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddFilmToDirectorDto {
    private Long filmId;
    private Long directorId;
}
