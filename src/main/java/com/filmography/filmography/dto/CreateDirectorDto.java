package com.filmography.filmography.dto;

import lombok.Data;

@Data
public class CreateDirectorDto {
    private String directorsFio;
    private String position;
}