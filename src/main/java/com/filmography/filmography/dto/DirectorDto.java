package com.filmography.filmography.dto;

import com.filmography.filmography.general.GenericDto;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.Set;

@Data
public class DirectorDto extends GenericDto {
    @NotBlank(message = "Обязательное поле для заполнения")
    private String directorsFio;
    private String position;
    private Set<Long> filmsIds;
}
