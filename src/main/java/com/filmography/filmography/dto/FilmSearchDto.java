package com.filmography.filmography.dto;

import com.filmography.filmography.model.Genre;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FilmSearchDto {
    private String filmTitle;
    private String directorsFio;
    private Genre genre;
}
