package com.filmography.filmography.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UpdateUserDto {
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    private String phone;
    private String address;
    private String email;
    private Long roleId;
}