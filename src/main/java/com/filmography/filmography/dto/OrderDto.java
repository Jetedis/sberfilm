package com.filmography.filmography.dto;

import com.filmography.filmography.general.GenericDto;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class OrderDto extends GenericDto {
    private Long userId;
    private Long filmId;
    private LocalDateTime rentDate;
    private Integer rentPeriod;
    private Boolean purchase;
    private LocalDate returnDate;
    private Integer count;
}
