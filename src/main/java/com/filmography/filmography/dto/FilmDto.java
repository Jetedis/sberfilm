package com.filmography.filmography.dto;

import com.filmography.filmography.general.GenericDto;
import com.filmography.filmography.model.Genre;
import lombok.Data;

import java.time.LocalDate;
import java.util.Set;;

@Data
public class FilmDto extends GenericDto {
    private String title;
    private LocalDate premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsIds;
    private Set<Long> ordersIds;
}

