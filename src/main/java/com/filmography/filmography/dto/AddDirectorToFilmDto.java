package com.filmography.filmography.dto;

import lombok.Data;


@Data
public class AddDirectorToFilmDto {
    private Long directorId;
}
