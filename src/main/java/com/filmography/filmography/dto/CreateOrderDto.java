package com.filmography.filmography.dto;

import lombok.Data;

import java.time.LocalDateTime;


@Data
public class CreateOrderDto {
    private Long userId;
    private Long filmId;
    private LocalDateTime rentDate;
    private Integer rentPeriod;
    private Boolean purchase;
}
