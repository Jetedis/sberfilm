package com.filmography.filmography.dto;

import lombok.Data;

@Data
public class CreateUserDto {
    private String login;
    private String password;
}