package com.filmography.filmography.dto;

import lombok.Data;

@Data
public class RentFilmDto {

    Long filmId;
    Long userId;
    Integer count;
    Integer rentPeriod;
}

