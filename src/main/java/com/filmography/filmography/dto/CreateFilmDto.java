package com.filmography.filmography.dto;

import com.filmography.filmography.model.Genre;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateFilmDto {
    private String title;
    private LocalDate premierYear;
    private String country;
    private Genre genre;
}
