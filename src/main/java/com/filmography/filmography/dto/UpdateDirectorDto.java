package com.filmography.filmography.dto;

import lombok.Data;

@Data
public class UpdateDirectorDto {
    private String directorsFio;
    private String position;
}
