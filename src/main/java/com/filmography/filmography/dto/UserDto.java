package com.filmography.filmography.dto;

import com.filmography.filmography.general.GenericDto;
import com.filmography.filmography.model.Role;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Data
public class UserDto extends GenericDto {
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    private String phone;
    private String address;
    private String email;
    private LocalDateTime createdWhen;
    private Long roleId;
    private Role role;
    private String changePasswordToken;
    private Set<Long> ordersIds;
}
