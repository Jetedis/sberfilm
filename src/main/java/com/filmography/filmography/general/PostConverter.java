package com.filmography.filmography.general;

import org.modelmapper.Converter;


public interface PostConverter<E extends GenericModel, D extends GenericDto> {

    default Converter<E, D> toDtoConverter() {
        return context -> {
            E source = context.getSource();
            D destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }

    default Converter<D, E> toEntityConverter() {
        return context -> {
            D source = context.getSource();
            E destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }

    void mapSpecificFields(E source, D destination);

    void mapSpecificFields(D source, E destination);
}


