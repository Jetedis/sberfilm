package com.filmography.filmography.general;

public interface CreateEntityMapper<D, E> {
    E toEntity(D obj);
}
