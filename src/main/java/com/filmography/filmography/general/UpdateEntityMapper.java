package com.filmography.filmography.general;

public interface UpdateEntityMapper<D, E> {
    E toEntity(D obj, E entity);
}
