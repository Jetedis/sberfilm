package com.filmography.filmography.general;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GenericService<T> {
    List<T> findAll();
    Page<T> findAll(Pageable pageable);
    T findById(Long id);
    T create(T obj);
    T update(T obj);
    void delete(Long id);
    void restore(Long id);
    List<T> findByCreatedBy(String createdBy);
    boolean existsById(Long id);
    void softDelete(Long id);
}