package com.filmography.filmography.general;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
public abstract class GenericServiceImpl<T extends GenericModel> implements GenericService<T> {

    private final GenericRepository<T> repository;

    public List<T> findAll() {
        return repository.findAll();
    }
    public Page<T> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public T findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Запись с id '%s' не найдена", id)));
    }

    public T create(T obj) {
        obj.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        obj.setCreatedWhen(LocalDateTime.now());
        return repository.save(obj);
    }

    public T update(T obj) {
        obj.setUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        obj.setUpdatedWhen(LocalDateTime.now());
        return repository.save(obj);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public void restore(Long id) {
        T object = findById(id);
        object.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setDeleted(false);
        object.setDeletedWhen(null);
        update(object);
    }
    public void softDelete(Long id) {
        T object = findById(id);
        object.setDeletedBy("ADMIN"); //TODO переделать с секурити
        object.setDeleted(true);
        object.setDeletedWhen(LocalDateTime.now());
        update(object);
    }

    public List<T> findByCreatedBy(String createdBy) {
        return repository.findByCreatedBy(createdBy);
    }

    public boolean existsById(Long id) {
        return repository.existsById(id);
    }
}
