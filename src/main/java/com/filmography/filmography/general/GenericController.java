package com.filmography.filmography.general;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
public abstract class GenericController<E extends GenericModel, D extends GenericDto, C, U> {

    private final GenericService<E> service;
    private final Mapper<E, D> mapper;

    @Operation(description = "Получить все записи")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<D>> findAll(HttpServletRequest request) {
        log.info("Получить все записи: {}", request.getRequestURL());
        return ResponseEntity.ok(mapper.toDtos(service.findAll()));
    }

    @Operation(description = "Получить запись по ID")
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> findById(@PathVariable Long id, HttpServletRequest request) {
        log.info("Получить запись по ID: {}", request.getRequestURL());
        return ResponseEntity.ok(mapper.toDto(service.findById(id)));
    }

    @Operation(description = "Создать новую запись")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> create(@RequestBody C obj, HttpServletRequest request) {
        log.info("Создать новую запись: {}", request.getRequestURL());
        log.info("obj: {}", obj);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(mapper.toDto(service.create(mapper.toEntity((D) obj))));
    }

    @Operation(description = "Редактировать запись")
    @PatchMapping(
            path = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<D> update(@PathVariable Long id, @RequestBody U obj, HttpServletRequest request) {
        log.info("Редактировать запись: {}", request.getRequestURL());
        log.info("obj: {}", obj);
        E entity = service.findById(id);
        return ResponseEntity.ok(mapper.toDto(service.update(mapper.toEntity((D) obj))));
    }

    @Operation(description = "Удалить запись по ID")
    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable Long id, HttpServletRequest request) {
        log.info("Удалить запись по ID: {}", request.getRequestURL());
        service.delete(id);
    }
}
