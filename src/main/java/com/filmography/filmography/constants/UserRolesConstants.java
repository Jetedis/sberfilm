package com.filmography.filmography.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String EDITOR = "EDITOR";
    String USER = "USER";

}

