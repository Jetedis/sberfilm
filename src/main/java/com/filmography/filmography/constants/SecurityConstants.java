package com.filmography.filmography.constants;

import java.util.List;

public interface SecurityConstants {
    class REST {
        public static List<String> FILMS_WHITE_LIST = List.of(
                "/rest/films",
                "/rest/films/search",
                "/rest/films/{id}");

        public static List<String> DIRECTORS_WHITE_LIST = List.of(
                "/rest/directors",
                "/rest/directors/search",
                "/rest/directors/getAll",
                "/rest/films/search/director",
                "/rest/directors/{id}");

        public static List<String> USERS_WHITE_LIST = List.of(
                "/rest/users/auth",
                "/rest/users/registration",
                "/rest/users/remember-password",
                "/rest/users/change-password");

        public static List<String> DIRECTORS_PERMISSION_LIST = List.of(
                "/rest/directors/add",
                "/rest/directors/update",
                "/rest/directors/delete/{id}",
                "/rest/directors/soft-delete/{id}",
                "/rest/directors/restore/{id}"
        );

        public static List<String> FILMS_PERMISSION_LIST = List.of(
                "/rest/films/add",
                "/rest/films/update",
                "/rest/films/delete/**",
                "/rest/films/delete/{id}",
                "/rest/films/download/{filmId}");

        public static List<String> USERS_PERMISSION_LIST = List.of(
                "/rest/rent/film/*");
    }

    List<String> RESOURCES_WHITE_LIST = List.of(
            "/resources/**",
            "/js/**",
            "/css/**",
            "/",
            // -- Swagger UI v3 (OpenAPI)
            "/swagger-ui/**",
            "/v3/api-docs/**",
            "/login",
            "/rest/users/auth",
            "/users/registration",
            "/users/remember-password",
            "/users/change-password",
            "/error");

    List<String> FILMS_WHITE_LIST = List.of(
            "/films/{filmId}",
            "/films/search",
            "/films/search/director",
            "/films"
    );


    List<String> RENT_WHITE_LIST = List.of(
            "/rent/get-film/*",
            "/rent/get-film",
            "/rent/user-films",
            "/rent/return-film/{id}"
    );


    List<String> DIRECTORS_WHITE_LIST = List.of(
            "/directors/{id}",
            "/directors/search",
            "/directors"
    );

    List<String> DIRECTORS_PERMISSION_LIST = List.of(
            "/directors/add",
            "/directors/update",
            "/directors/delete",
            "/directors/get-film/*",
            "/directors/add",
            "/directors/add-film/{id}",
            "/directors/download/*",
            "/rest/directors/soft-delete/{id}"
    );

    List<String> FILMS_PERMISSION_LIST = List.of(
            "/films/add",
            "/films/update",
            "/films/delete",
            "/publish/get-film/*",
            "/films/add",
            "/films/download/*"

    );

    List<String> USERS_PERMISSION_LIST = List.of(
            "/users",
            "/users/add",
            "/users/delete",
            "/users/add-editor"
    );

    List<String> USERS_WHITE_LIST = List.of(
            "/users/profile/{id}",
            "/users/profile/update/{id}"

    );

    List<String> USERS_REST_WHITE_LIST = List.of("/users/auth");
}
