package com.filmography.filmography.MVC.Controller;

import com.filmography.filmography.dto.AddFilmToDirectorDto;
import com.filmography.filmography.dto.DirectorDto;
import com.filmography.filmography.mapper.DirectorMapper;
import com.filmography.filmography.model.Director;
import com.filmography.filmography.service.DirectorService;
import com.filmography.filmography.service.FilmService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/directors")
@Slf4j
public class MVCDirectorController {

    private final DirectorMapper directorMapper;
    private final DirectorService directorService;
    private final FilmService filmService;

    public MVCDirectorController(DirectorMapper directorMapper, DirectorService directorService, FilmService filmService) {
        this.directorMapper = directorMapper;
        this.directorService = directorService;
        this.filmService = filmService;
    }

    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorsFio"));
        Page<Director> directorPage = directorService.findAll(pageRequest);
        Page<DirectorDto> directorDtoPage = new PageImpl<>(directorMapper.toDtos(directorPage.getContent()), pageRequest, directorPage.getTotalElements());
        model.addAttribute("directors", directorDtoPage);
        return "directors/viewAllDirectors";
    }

    @GetMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDto directorDto) {

        return "directors/addDirector";
    }

    @PostMapping("/add")
    public String create(
            @ModelAttribute("directorForm") @Valid DirectorDto directorDto,
            BindingResult result
    ) {
        if(result.hasErrors()) {
            return "/directors/addDirector";
        } else {
            directorService.create(directorMapper.toEntity(directorDto));
            return "redirect:/directors";
        }
    }


    @GetMapping("/delete/{id}")
    public String safeDelete(@PathVariable Long id) {
        directorService.softDelete(id);
        return "redirect:/directors";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        directorService.restore(id);
        return "redirect:/directors";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable Long id) {
        model.addAttribute("director", directorService.findById(id));
        return "directors/updateDirector";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("directorForm") DirectorDto directorDto) {
        directorService.update(directorMapper.toEntity(directorDto));
        return "redirect:/directors";
    }

    @PostMapping("/search")
    public String search(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            @ModelAttribute("directorSearchForm") DirectorDto directorDto,
            Model model
    ) {
        if (!StringUtils.hasText(directorDto.getDirectorsFio()) || !StringUtils.hasLength(directorDto.getDirectorsFio())) {
            return "redirect:/directors";
        } else {
            PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorsFio"));
            Page<Director> directorPage = directorService.searchDirectors(directorDto.getDirectorsFio().trim(), pageRequest);
            Page<DirectorDto> directorDtoPage = new PageImpl<>(directorMapper.toDtos(directorPage.getContent()), pageRequest, directorPage.getTotalElements());
            model.addAttribute("directors", directorDtoPage);
            return "directors/viewAllDirectors";
        }
    }

    @GetMapping("/add-film/{directorId}")
    public String addFilm(@PathVariable Long directorId,
                          Model model) {
        model.addAttribute("films", filmService.findAll());
        model.addAttribute("directorId", directorId);
        model.addAttribute("director", directorService.findById(directorId).getDirectorsFio());
        return "directors/addDirectorFilm";
    }

    @PostMapping("/add-film")
    public String addFilm(@ModelAttribute("directorFilmForm") AddFilmToDirectorDto addFilmDTO) {
        directorService.addFilm(addFilmDTO);
        return "redirect:/directors";
    }

    @GetMapping("/{id}")
    public String findById (@PathVariable Long id, Model model) {
        model.addAttribute("director", directorMapper.toDto(directorService.findById(id)));
        return "directors/viewDirector";
    }



}

