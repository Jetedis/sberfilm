package com.filmography.filmography.MVC.Controller;

import com.filmography.filmography.dto.FilmDto;
import com.filmography.filmography.dto.FilmSearchDto;
import com.filmography.filmography.mapper.FilmMapper;
import com.filmography.filmography.service.DirectorService;
import com.filmography.filmography.service.FilmService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/films")
public class MVCFilmController {

    private final FilmService service;
    private final FilmMapper mapper;
    private final DirectorService directorService;

    public MVCFilmController(FilmService service, FilmMapper mapper, DirectorService directorService) {
        this.service = service;
        this.mapper = mapper;
        this.directorService = directorService;
    }
    @GetMapping("")
    public String getAll(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int pageSize,
            Model model
    ) {
        return "films/viewAllFilms";
    }

    @GetMapping("/add")
    public String addFilm() {
        return "films/addFilm";
    }

    @PostMapping("/add")
    public String addFilm(@ModelAttribute("filmForm") FilmDto filmDto) {
        service.create(mapper.toEntity(filmDto));
        return "redirect:/films";
    }

    @GetMapping("/{filmId}")
    public String viewOneFilm(@PathVariable Long filmId, Model model) {
        model.addAttribute("film", mapper.toDto(service.findById(filmId)));
        return "/films/viewFilm";
    }


    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model) {
        model.addAttribute("film", mapper.toDto(service.findById(id)));
        return "films/updateFilm";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("filmForm") FilmDto filmDto) {
        service.update(mapper.toEntity(filmDto));
        return "redirect:/films";
    }


    @GetMapping("/add-director/{filmId}")
    public String addDirector(@PathVariable Long filmId,
                          Model model) {
        return "directors/addFilmDirector";
    }
    @PostMapping("/search/director")
    public String searchFilms(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @ModelAttribute("directorSearchForm") FilmSearchDto directorDto,
            Model model
    ) {
        FilmSearchDto filmSearchDto = new FilmSearchDto();
        filmSearchDto.setDirectorsFio(directorDto.getDirectorsFio());
        return searchFilms(page, size, filmSearchDto, model);
    }


}
