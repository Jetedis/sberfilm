package com.filmography.filmography.MVC.Controller;

import com.filmography.filmography.dto.RentFilmDto;
import com.filmography.filmography.mapper.FilmMapper;
import com.filmography.filmography.service.FilmService;
import com.filmography.filmography.service.OrderService;
import com.filmography.filmography.service.userDetails.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/rent")
public class MVCOrderController {

    private final FilmService filmService;
    private final FilmMapper filmMapper;
    private final OrderService service;

    public MVCOrderController(FilmService filmService, FilmMapper filmMapper, OrderService service) {
        this.filmService = filmService;
        this.filmMapper = filmMapper;
        this.service = service;
    }

    @GetMapping("/get-film/{filmId}")
    public String getFilm(@PathVariable Long filmId, Model model) {
        model.addAttribute("film", filmMapper.toDto(filmService.findById(filmId)));
        return "userFilms/getFilm";
    }

    @PostMapping("/get-film")
    public String getFilm(@ModelAttribute("publishForm") RentFilmDto rentFilmDto) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        rentFilmDto.setUserId(Long.valueOf(customUserDetails.getUserId()));
        service.rentFilm(rentFilmDto);
        return "redirect:/films";
    }

    @GetMapping("/user-films")
    public String getUserFilms(Model model) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("rentFilms", service.findAllByUserId(Long.valueOf(customUserDetails.getUserId())));
        return "userFilms/viewAllUserFilms";
    }

    @GetMapping("return-film/{id}")
    public String returnFilm(@PathVariable Long id) {
        service.returnFilm(id);
        return "redirect:/rent/user-films";
    }
}
