package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.UpdateOrderDto;
import com.filmography.filmography.model.Film;
import com.filmography.filmography.model.Order;
import com.filmography.filmography.service.FilmService;
import com.filmography.filmography.general.UpdateEntityMapper;
import com.filmography.filmography.model.User;
import com.filmography.filmography.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Objects;


@Component
@RequiredArgsConstructor
public class UpdateOrderMapper implements UpdateEntityMapper<UpdateOrderDto, Order> {

    private final UserService userService;

    private final FilmService filmService;

    @Override
    public Order toEntity(UpdateOrderDto obj, Order entity) {
        Long userId = obj.getUserId();
        if (Objects.nonNull(userId)) {
            User user = userService.findById(userId);
            entity.setUser(user);
        }

        Long filmId = obj.getFilmId();
        if (Objects.nonNull(filmId)) {
            Film film = filmService.findById(filmId);
            entity.setFilm(film);
        }

        LocalDateTime rentDate = obj.getRentDate();
        if (Objects.nonNull(rentDate)) {
            entity.setRentDate(rentDate);
        }

        Integer rentPeriod = obj.getRentPeriod();
        if (Objects.nonNull(rentPeriod)) {
            entity.setRentPeriod(rentPeriod);
        }

        Boolean purchase = obj.getPurchase();
        if (Objects.nonNull(purchase)) {
            entity.setPurchase(purchase);
        }

        return entity;
    }
}
