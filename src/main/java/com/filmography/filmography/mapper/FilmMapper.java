package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.FilmDto;
import com.filmography.filmography.model.Director;
import com.filmography.filmography.model.Film;
import com.filmography.filmography.service.DirectorService;
import com.filmography.filmography.general.GenericMapper;
import com.filmography.filmography.general.GenericModel;
import com.filmography.filmography.general.PostConverter;
import com.filmography.filmography.model.Order;
import com.filmography.filmography.service.OrderService;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDto> implements PostConverter<Film, FilmDto> {

    private final ModelMapper mapper;

    private final DirectorService directorService;

    private final OrderService orderService;

    public FilmMapper(ModelMapper mapper, DirectorService directorService, OrderService orderService) {
        super(mapper, Film.class, FilmDto.class);
        this.mapper = mapper;
        this.directorService = directorService;
        this.orderService = orderService;
    }

    @PostConstruct
    private void postConstruct() {
        mapper.createTypeMap(Film.class, FilmDto.class)
                .addMappings(m -> m.skip(FilmDto::setDirectorsIds))
                .addMappings(m -> m.skip(FilmDto::setOrdersIds))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(FilmDto.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors))
                .addMappings(m -> m.skip(Film::setOrders))
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Film source, FilmDto destination) {
        Set<Director> directors = source.getDirectors();
        if (Objects.nonNull(directors)) {
            Set<Long> directorsIds = directors.stream().map(GenericModel::getId).collect(Collectors.toSet());
            destination.setDirectorsIds(directorsIds);
        }

        Set<Order> orders = source.getOrders();
        if (Objects.nonNull(orders)) {
            Set<Long> ordersIds = orders.stream().map(GenericModel::getId).collect(Collectors.toSet());
            destination.setOrdersIds(ordersIds);
        }
    }

    @Override
    public void mapSpecificFields(FilmDto source, Film destination) {
        Set<Long> directorsIds = source.getDirectorsIds();
        if (Objects.nonNull(directorsIds)) {
            Set<Director> directors = new HashSet<>(directorService.findAllById(directorsIds));
            destination.setDirectors(directors);
        }

        Set<Long> ordersIds = source.getOrdersIds();
        if (Objects.nonNull(ordersIds)) {
            Set<Order> orders = new HashSet<>(orderService.findAllById(ordersIds));
            destination.setOrders(orders);
        }
    }
}

