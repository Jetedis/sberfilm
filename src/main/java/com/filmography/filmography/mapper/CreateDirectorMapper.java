package com.filmography.filmography.mapper;

import com.filmography.filmography.model.Director;
import com.filmography.filmography.general.CreateEntityMapper;
import com.filmography.filmography.dto.CreateDirectorDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;
@Component
@RequiredArgsConstructor
public class CreateDirectorMapper implements CreateEntityMapper<CreateDirectorDto, Director> {

    private final ModelMapper mapper;

    @Override
    public Director toEntity(CreateDirectorDto obj) {
        return mapper.map(obj, Director.class);
    }
}
