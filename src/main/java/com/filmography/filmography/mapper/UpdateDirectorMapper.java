package com.filmography.filmography.mapper;

import com.filmography.filmography.general.UpdateEntityMapper;
import com.filmography.filmography.dto.UpdateDirectorDto;
import com.filmography.filmography.model.Director;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class UpdateDirectorMapper implements UpdateEntityMapper<UpdateDirectorDto, Director> {

    @Override
    public Director toEntity(UpdateDirectorDto obj, Director entity) {
        String directorsFio = obj.getDirectorsFio();
        if (Objects.nonNull(directorsFio)) {
            entity.setDirectorsFio(directorsFio);
        }

        String position = obj.getPosition();
        if (Objects.nonNull(position)) {
            entity.setPosition(position);
        }

        return entity;
    }
}
