package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.CreateUserDto;
import com.filmography.filmography.general.CreateEntityMapper;
import com.filmography.filmography.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;


@Component
@RequiredArgsConstructor
public class CreateUserMapper implements CreateEntityMapper<CreateUserDto, User> {

    private final ModelMapper mapper;

    @Override
    public User toEntity(CreateUserDto obj) {
        User user = mapper.map(obj, User.class);
        user.setCreatedWhen(LocalDateTime.now());
        return user;
    }
}