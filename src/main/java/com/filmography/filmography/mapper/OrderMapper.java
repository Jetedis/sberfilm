package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.OrderDto;
import com.filmography.filmography.model.Film;
import com.filmography.filmography.model.Order;
import com.filmography.filmography.service.FilmService;
import com.filmography.filmography.general.GenericMapper;
import com.filmography.filmography.general.PostConverter;
import com.filmography.filmography.model.User;
import com.filmography.filmography.service.UserService;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;

import java.util.Objects;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDto> implements PostConverter<Order, OrderDto> {

    private final ModelMapper mapper;

    private final UserService userService;

    private final FilmService filmService;

    protected OrderMapper(ModelMapper mapper, UserService userService, FilmService filmService) {
        super(mapper, Order.class, OrderDto.class);
        this.mapper = mapper;
        this.userService = userService;
        this.filmService = filmService;
    }

    @PostConstruct
    private void postConstruct() {
        mapper.createTypeMap(Order.class, OrderDto.class)
                .addMappings(m -> m.skip(OrderDto::setUserId))
                .addMappings(m -> m.skip(OrderDto::setFilmId))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(OrderDto.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser))
                .addMappings(m -> m.skip(Order::setFilm))
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Order source, OrderDto destination) {
        User user = source.getUser();
        if (Objects.nonNull(user)) {
            destination.setUserId(user.getId());
        }
        Film film = source.getFilm();
        if (Objects.nonNull(film)) {
            destination.setFilmId(film.getId());
        }
    }

    @Override
    public void mapSpecificFields(OrderDto source, Order destination) {
        Long userId = source.getUserId();
        if (Objects.nonNull(userId)) {
            destination.setUser(userService.findById(userId));
        }

        Long filmId = source.getFilmId();
        if (Objects.nonNull(filmId)) {
            destination.setFilm(filmService.findById(filmId));
        }
    }
}

