package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.UpdateFilmDto;
import com.filmography.filmography.general.UpdateEntityMapper;
import com.filmography.filmography.model.Film;
import com.filmography.filmography.model.Genre;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Objects;

@Component
public class UpdateFilmMapper implements UpdateEntityMapper<UpdateFilmDto, Film> {

    @Override
    public Film toEntity(UpdateFilmDto obj, Film entity) {
        String title = obj.getTitle();
        if (Objects.nonNull(title)) {
            entity.setTitle(title);
        }

        LocalDate premierYear = LocalDate.from(obj.getPremierYear());
        if (Objects.nonNull(premierYear)) {
            entity.setPremierYear(premierYear);
        }

        String country = obj.getCountry();
        if (Objects.nonNull(country)) {
            entity.setCountry(country);
        }

        Genre genre = obj.getGenre();
        if (Objects.nonNull(genre)) {
            entity.setGenre(genre);
        }

        return entity;
    }
}

