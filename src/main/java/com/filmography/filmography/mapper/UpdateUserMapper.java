package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.UpdateUserDto;
import com.filmography.filmography.general.UpdateEntityMapper;
import com.filmography.filmography.model.Role;
import com.filmography.filmography.model.User;
import com.filmography.filmography.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class UpdateUserMapper implements UpdateEntityMapper<UpdateUserDto, User> {

    private final RoleService roleService;

    @Override
    public User toEntity(UpdateUserDto obj, User entity) {
        String password = obj.getPassword();
        if (Objects.nonNull(password)) {
            entity.setPassword(password);
        }

        String firstName = obj.getFirstName();
        if (Objects.nonNull(firstName)) {
            entity.setFirstName(firstName);
        }

        String lastName = obj.getLastName();
        if (Objects.nonNull(lastName)) {
            entity.setLastName(lastName);
        }

        String middleName = obj.getMiddleName();
        if (Objects.nonNull(middleName)) {
            entity.setMiddleName(middleName);
        }

        LocalDate birthDate = obj.getBirthDate();
        if (Objects.nonNull(birthDate)) {
            entity.setBirthDate(birthDate);
        }

        String phone = obj.getPhone();
        if (Objects.nonNull(phone)) {
            entity.setPhone(phone);
        }

        String address = obj.getAddress();
        if (Objects.nonNull(address)) {
            entity.setAddress(address);
        }

        String email = obj.getEmail();
        if (Objects.nonNull(email)) {
            entity.setEmail(email);
        }

        Long roleId  = obj.getRoleId();
        if (Objects.nonNull(roleId)) {
            Role role = roleService.findById(roleId);
            entity.setRole(role);
        }

        return entity;
    }
}
