package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.UserDto;
import com.filmography.filmography.general.GenericMapper;
import com.filmography.filmography.general.PostConverter;
import com.filmography.filmography.model.Order;
import com.filmography.filmography.model.User;
import com.filmography.filmography.service.OrderService;
import com.filmography.filmography.model.Role;
import com.filmography.filmography.service.RoleService;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDto> implements PostConverter<User, UserDto> {

    private final ModelMapper mapper;

    private final RoleService roleService;

    private final OrderService orderService;

    protected UserMapper(ModelMapper mapper, RoleService roleService, OrderService orderService) {
        super(mapper, User.class, UserDto.class);
        this.mapper = mapper;
        this.roleService = roleService;
        this.orderService = orderService;
    }

    @PostConstruct
    private void postConstruct() {
        mapper.createTypeMap(User.class, UserDto.class)
                .addMappings(m -> m.skip(UserDto::setRoleId))
                .addMappings(m -> m.skip(UserDto::setOrdersIds))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(UserDto.class, User.class)
                .addMappings(m -> m.skip(User::setRole))
                .addMappings(m -> m.skip(User::setOrders))
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(User source, UserDto destination) {
        Role role = source.getRole();
        if (Objects.nonNull(role)) {
            destination.setRoleId(role.getId());
        }
        Long userId = source.getId();
        if (Objects.nonNull(userId)) {
            List<Order> orders = orderService.findAllByUserId(userId);
            Set<Long> ordersIds = orders.stream().map(o -> o.getId()).collect(Collectors.toSet());
            destination.setOrdersIds(ordersIds);
        }
    }

    @Override
    public void mapSpecificFields(UserDto source, User destination) {
        Long roleId = source.getRoleId();
        if (Objects.nonNull(roleId)) {
            destination.setRole(roleService.findById(roleId));
        }
        Set<Long> ordersIds = source.getOrdersIds();
        if (Objects.nonNull(ordersIds)) {
            Set<Order> orders = new HashSet<>(orderService.findAllById(ordersIds));
            destination.setOrders(orders);
        }
    }
}

