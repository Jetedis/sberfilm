package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.CreateOrderDto;
import com.filmography.filmography.model.Order;
import com.filmography.filmography.service.FilmService;
import com.filmography.filmography.general.CreateEntityMapper;
import com.filmography.filmography.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class CreateOrderMapper implements CreateEntityMapper<CreateOrderDto, Order> {

    private final ModelMapper mapper;

    private final UserService userService;

    private final FilmService filmService;

    @Override
    public Order toEntity(CreateOrderDto obj) {
        mapper.createTypeMap(CreateOrderDto.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser))
                .addMappings(m -> m.skip(Order::setFilm))
                .setPostConverter(context -> {
                    CreateOrderDto source = context.getSource();
                    Long userId = source.getUserId();
                    Long filmId = source.getFilmId();
                    Order destination = context.getDestination();
                    if (Objects.nonNull(userId)) {
                        destination.setUser(userService.findById(userId));
                    }
                    if (Objects.nonNull(filmId)) {
                        destination.setFilm(filmService.findById(filmId));
                    }
                    return destination;
                });
        return mapper.map(obj, Order.class);
    }
}

