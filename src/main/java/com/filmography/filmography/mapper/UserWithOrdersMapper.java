package com.filmography.filmography.mapper;

import com.filmography.filmography.model.User;
import com.filmography.filmography.dto.UserWithOrdersDto;
import com.filmography.filmography.general.GenericMapper;
import com.filmography.filmography.general.PostConverter;
import com.filmography.filmography.model.Role;
import com.filmography.filmography.service.RoleService;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;

import java.util.Objects;

@Component
public class UserWithOrdersMapper extends GenericMapper<User, UserWithOrdersDto> implements PostConverter<User, UserWithOrdersDto> {

    private final ModelMapper mapper;

    private final RoleService roleService;

    protected UserWithOrdersMapper(ModelMapper mapper, RoleService roleService) {
        super(mapper, User.class, UserWithOrdersDto.class);
        this.mapper = mapper;
        this.roleService = roleService;
    }

    @PostConstruct
    private void postConstruct() {
        mapper.createTypeMap(User.class, UserWithOrdersDto.class)
                .addMappings(m -> m.skip(UserWithOrdersDto::setRoleId))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(UserWithOrdersDto.class, User.class)
                .addMappings(m -> m.skip(User::setRole))
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(User source, UserWithOrdersDto destination) {
        Role role = source.getRole();
        if (Objects.nonNull(role)) {
            destination.setRoleId(role.getId());
        }
    }

    @Override
    public void mapSpecificFields(UserWithOrdersDto source, User destination) {
        Long roleId = source.getRoleId();
        if (Objects.nonNull(roleId)) {
            destination.setRole(roleService.findById(roleId));
        }
    }
}

