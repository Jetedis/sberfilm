package com.filmography.filmography.mapper;

import com.filmography.filmography.dto.CreateFilmDto;
import com.filmography.filmography.general.CreateEntityMapper;
import com.filmography.filmography.model.Film;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;

@Component
@RequiredArgsConstructor
public class CreateFilmMapper implements CreateEntityMapper<CreateFilmDto, Film> {

    private final ModelMapper mapper;

    @Override
    public Film toEntity(CreateFilmDto obj) {
        return mapper.map(obj, Film.class);
    }
}
