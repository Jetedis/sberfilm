package com.filmography.filmography.mapper;

import com.filmography.filmography.model.Film;
import com.filmography.filmography.service.FilmService;
import com.filmography.filmography.general.GenericMapper;
import com.filmography.filmography.general.GenericModel;
import com.filmography.filmography.general.PostConverter;
import com.filmography.filmography.dto.DirectorDto;
import com.filmography.filmography.model.Director;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapper<Director, DirectorDto> implements PostConverter<Director, DirectorDto> {

    private final ModelMapper mapper;

    private final FilmService filmService;

    protected DirectorMapper(ModelMapper mapper, FilmService filmService) {
        super(mapper, Director.class, DirectorDto.class);
        this.mapper = mapper;
        this.filmService = filmService;
    }

    @PostConstruct
    private void postConstruct() {
        mapper.createTypeMap(Director.class, DirectorDto.class)
                .addMappings(m -> m.skip(DirectorDto::setFilmsIds))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(DirectorDto.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms))
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Director source, DirectorDto destination) {
        Set<Film> films = source.getFilms();
        if (Objects.nonNull(films)) {
            Set<Long> filmsIds = films.stream().map(GenericModel::getId).collect(Collectors.toSet());
            destination.setFilmsIds(filmsIds);
        }
    }

    @Override
    public void mapSpecificFields(DirectorDto source, Director destination) {
        Set<Long> filmsIds = source.getFilmsIds();
        if (Objects.nonNull(filmsIds)) {
            Set<Film> films = new HashSet<>(filmService.findAllById(filmsIds));
            destination.setFilms(films);
        }
    }
}
