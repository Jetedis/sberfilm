package com.filmography.filmography.service;

import com.filmography.filmography.dto.RentFilmDto;
import com.filmography.filmography.general.GenericService;
import com.filmography.filmography.model.Order;

import java.util.List;

public interface OrderService extends GenericService<Order> {
    List<Order> findAllById(Iterable<Long> ids);
    List<Order> findAllByUserId(Long id);
    void returnFilm(Long id);
    Order rentFilm(RentFilmDto rentFilmDto);
}

