package com.filmography.filmography.service;

import com.filmography.filmography.model.User;
import com.filmography.filmography.general.GenericService;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserService extends GenericService<User> {
    User create(User object);
    User update(User object);
    User getUserByLogin(String login);
    User getUserByEmail(String email);
    User createEditor(User object);
    boolean checkPassword(String password, UserDetails foundUser);
    void sendChangePasswordEmail(User user);
    void changePassword(String uuid, String password);
}