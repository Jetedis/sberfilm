package com.filmography.filmography.service;

import com.filmography.filmography.dto.AddFilmToDirectorDto;
import com.filmography.filmography.general.GenericService;
import com.filmography.filmography.model.Director;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DirectorService extends GenericService<Director> {
    List<Director> findAllById(Iterable<Long> ids);
    void addFilm(AddFilmToDirectorDto addFilmDto);
    Page<Director> searchDirectors(final String fio, Pageable pageable);
    void softDelete(Long id);
}
