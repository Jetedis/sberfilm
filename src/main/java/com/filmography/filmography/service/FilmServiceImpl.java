package com.filmography.filmography.service;

import com.filmography.filmography.repository.FilmRepository;
import com.filmography.filmography.general.GenericServiceImpl;
import com.filmography.filmography.model.Film;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmServiceImpl extends GenericServiceImpl<Film> implements FilmService {

    private final FilmRepository repository;

    protected FilmServiceImpl(FilmRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public List<Film> findAllById(Iterable<Long> ids) {
        return repository.findAllById(ids);
    }

}

