package com.filmography.filmography.service;


import com.filmography.filmography.model.Role;

public interface RoleService {
    Role findById(Long id);
    Role getByTitle(String title);
}