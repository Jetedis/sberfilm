package com.filmography.filmography.service;

import com.filmography.filmography.general.GenericService;
import com.filmography.filmography.model.Film;

import java.util.List;

public interface FilmService extends GenericService<Film> {
    List<Film> findAllById(Iterable<Long> ids);
}
