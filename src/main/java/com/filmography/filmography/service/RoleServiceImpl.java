package com.filmography.filmography.service;

import com.filmography.filmography.model.Role;
import com.filmography.filmography.repository.RoleRepository;
import com.filmography.filmography.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;


@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Override
    public Role findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Роль с ID '%s' не найдена", id)));
    }

    public Role getByTitle(String title) {
        return repository.getRoleByTitle(title);
    }
}
