package com.filmography.filmography.service;

import com.filmography.filmography.dto.AddFilmToDirectorDto;
import com.filmography.filmography.general.GenericServiceImpl;
import com.filmography.filmography.model.Director;
import com.filmography.filmography.repository.DirectorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class DirectorServiceImpl extends GenericServiceImpl<Director> implements DirectorService {

    private final DirectorRepository repository;
    private final FilmService filmService;

    protected DirectorServiceImpl(DirectorRepository repository, FilmService filmService) {
        super(repository);
        this.repository = repository;
        this.filmService = filmService;
    }
    @Override
    @Secured(value = "ROLE_EDITOR")
    public void softDelete(Long id) {
        log.error("SOFT");
        Director director = findById(id);
        boolean directorCanBeDeleted = repository.checkDirectorForDeletion(id);
        if(directorCanBeDeleted) {
            director.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
            director.setDeleted(true);
            director.setDeletedWhen(LocalDateTime.now());
            update(director);
        }
    }

    public Page<Director> searchDirectors(final String fio, Pageable pageable) {
        return repository.findAllByDirectorsFioContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);
    }

    public void addFilm(AddFilmToDirectorDto addFilmDto) {
        Director director = findById(addFilmDto.getDirectorId());
        director.getFilms().add(filmService.findById(addFilmDto.getFilmId()));
        update(director);
    }
    @Override
    public List<Director> findAllById(Iterable<Long> ids) {
        return repository.findAllById(ids);
    }
}

