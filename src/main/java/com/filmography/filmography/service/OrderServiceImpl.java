package com.filmography.filmography.service;

import com.filmography.filmography.dto.RentFilmDto;
import com.filmography.filmography.model.Film;
import com.filmography.filmography.model.Order;
import com.filmography.filmography.model.User;
import com.filmography.filmography.repository.FilmRepository;
import com.filmography.filmography.repository.OrderRepository;
import com.filmography.filmography.service.OrderService;
import org.springframework.stereotype.Service;
import com.filmography.filmography.general.GenericServiceImpl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderServiceImpl extends GenericServiceImpl<Order> implements OrderService {

    private final UserService userService;
    private final FilmService filmService;
    private final FilmRepository filmRepository;
    private final OrderRepository repository;

    protected OrderServiceImpl(UserService userService, FilmService filmService,
                           FilmRepository filmRepository, OrderRepository repository) {
        super(repository);
        this.userService = userService;
        this.filmService = filmService;
        this.filmRepository = filmRepository;
        this.repository = repository;
    }

    @Override
    public List<Order> findAllById(Iterable<Long> ids) {
        return repository.findAllById(ids);
    }

    @Override
    public List<Order> findAllByUserId(Long id) {
        return repository.findAllByUserId(id);
    }
    public Order rentFilm(RentFilmDto rentFilmDto) {
        User user = userService.findById(rentFilmDto.getUserId());
        Film film = filmService.findById(rentFilmDto.getFilmId());
        if(film.getCount() - rentFilmDto.getCount() < 0) {
            return null;
        }
        film.setCount(film.getCount() - rentFilmDto.getCount());
        filmService.update(film);
        Order order = Order.builder()
                .rentDate(LocalDateTime.now())
                .purchase(false)
                .count(rentFilmDto.getCount())
                .returnDate(LocalDate.now().plusDays(rentFilmDto.getRentPeriod()))
                .rentPeriod(rentFilmDto.getRentPeriod())
                .user(user)
                .film(film)
                .build();
        return repository.save(order);
    }

    public void returnFilm(Long id) {
        Order order = findById(id);
        order.setPurchase(true);
        order.setReturnDate(LocalDate.now());
        Film film = order.getFilm();
        film.setCount(film.getCount() + order.getCount());
        update(order);
        filmService.update(film);
    }
}

